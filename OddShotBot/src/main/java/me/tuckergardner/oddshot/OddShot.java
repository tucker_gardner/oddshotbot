package me.tuckergardner.oddshot;

import net.dean.jraw.models.Submission;

public class OddShot {

	private int timestamp;
	private String redditID;
	private String channelName;
	private Submission submission;
	
	public OddShot(int time, String id, String channel, Submission sub){
		timestamp = time;
		redditID = id;
		channelName = channel;
		submission = sub;
	}
	public String getID(){
		return redditID;
	}
	
	public int getTime(){
		return timestamp;
	}
	public String getChannel(){
		return channelName;
	}
	public Submission getSubmission(){
		return submission;
	}
}
