package me.tuckergardner.oddshot;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import net.dean.jraw.ApiException;
import net.dean.jraw.RedditClient;
import net.dean.jraw.http.NetworkException;
import net.dean.jraw.http.UserAgent;
import net.dean.jraw.http.oauth.Credentials;
import net.dean.jraw.http.oauth.OAuthData;
import net.dean.jraw.http.oauth.OAuthException;
import net.dean.jraw.managers.AccountManager;
import net.dean.jraw.managers.ModerationManager;
import net.dean.jraw.models.Listing;
import net.dean.jraw.models.Submission;
import net.dean.jraw.paginators.Sorting;
import net.dean.jraw.paginators.SubredditPaginator;

public class App {
	private static Properties prop = new Properties();
	private static FileInputStream input;
	private static String username;
	private static String password;
	private static String clientID;
	private static String clientSecret;
	private static String subreddit;

	public static void main(String[] args) {

		// Load file properties
		try {
			input = new FileInputStream("config.properties");

			// loads properties file
			prop.load(input);
			username = prop.getProperty("username", "");
			password = prop.getProperty("password", "");
			clientID = prop.getProperty("clientID", "");
			clientSecret = prop.getProperty("clientSecret", "");
			subreddit = prop.getProperty("subreddit", "");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		try {
			// initialize a gui
			final BotGUI gui = new BotGUI();
			// Create a user agent
			final UserAgent myUserAgent = UserAgent.of("desktop",
					"me.tuckergardner.oddshot", "0.1", username);

			// Create and start the timer
			Timer mTimer = new Timer();
			mTimer.scheduleAtFixedRate(new TimerTask() {

				// Create Clients and variables needed to interact with the
				// reddit library
				long authTime = 0;
				RedditClient redditClient = new RedditClient(myUserAgent);
				Credentials credentials = Credentials.script(username,
						password, clientID, clientSecret);
				OAuthData authData = redditClient.getOAuthHelper().easyAuth(
						credentials);
				SubredditPaginator newCSGO = new SubredditPaginator(
						redditClient, subreddit);

				ArrayList<OddShot> shots = new ArrayList<OddShot>();
				ModerationManager modManager = new ModerationManager(
						redditClient);
				AccountManager accountManager = new AccountManager(redditClient);

				public void run() {
					// if we're not authenticated, or it's expired, authenticate
					if (!redditClient.isAuthenticated()) {
						redditClient.authenticate(authData);
						authTime = System.currentTimeMillis();
						gui.log("Authenticated");

					} else if (redditClient.isAuthenticated()
							&& (System.currentTimeMillis() - authTime > (55 * 60 * 1000))) {
						try {
							redditClient.getOAuthHelper().revokeAccessToken(credentials);
							redditClient.deauthenticate();
							authData = redditClient.getOAuthHelper().easyAuth(
									credentials);
							redditClient.authenticate(authData);
							authTime = System.currentTimeMillis();
							gui.log("Reauthenticated");
						} catch (NetworkException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (OAuthException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					// reset the subreddit paginator, reinitialize it, and set
					// the sorting to get /new
					newCSGO.reset();
					newCSGO = new SubredditPaginator(redditClient, subreddit);
					newCSGO.setSorting(Sorting.NEW);

					// Get all the submissions
					Listing<Submission> submissions = newCSGO.next();

					// Iterate through all of the submissions
					for (Submission s : submissions) {
						String url = s.getUrl();

						// if it's an oddshot link
						if (url.contains("://oddshot.tv")) {
							// parse the channel from the url
							int channelStart = url.indexOf("shot/") + 5;
							int channelEnd = url.lastIndexOf("-");
							String channel = url.substring(channelStart,
									channelEnd);
							// cut off the year/day/hour from the timestamp
							String timestampStr = url
									.substring(channelEnd + 11);

							// if there are less than 100 ms in the timestamp,
							// remove two digits
							if (timestampStr.length() == 6) {
								timestampStr = timestampStr.substring(0,
										timestampStr.length() - 2);

							}// else remove 3 digits
							else if (timestampStr.length() == 7) {

								timestampStr = timestampStr.substring(0,
										timestampStr.length() - 3);
							}
							// parse the minutes and seconds portion into a long
							long timestamp = Long.parseLong(timestampStr);
							// get the minutes and seconds
							int minutes = (int) (timestamp / 100);
							int seconds = (int) (timestamp % 100);
							// convert the minutes into seconds
							seconds = (minutes * 60) + seconds;
							// if the oddshot overlaps, we won't add it to the
							// list of shots
							boolean duplicate = false;

							// iterate through all shots in this set of
							// submissions
							for (OddShot o : shots) {

								// calc the difference between the two shots
								int difference = (Math.abs(o.getTime()
										- seconds));

								// if it's less than 40 seconds, on the same
								// channel, and isnt the same post, it's
								// overlapping
								if (channel.equalsIgnoreCase(o.getChannel())
										&& difference < 40
										&& (!s.getFullName().equalsIgnoreCase(
												o.getID()))) {

									try {
										// comment on the post saying it's a
										// duplicate, then delete the post
										// the author will get the message but
										// the post will stay removed
										accountManager
												.reply(o.getSubmission(),
														"Your submission has been detected as an overlapping shot and will be removed.");
										modManager.delete(o.getID());
										// since we're removing the newer post,
										// and we iterate from newest to oldest,
										// we're deleting the already stored
										// shot
										shots.remove(o);
									} catch (NetworkException e) {

										e.printStackTrace();
									} catch (ApiException e) {

										e.printStackTrace();
									}
									gui.log("Overlapping shot: " + url);
									duplicate = true;
									break;
								} else {
									duplicate = false;
								}
							}
							if (!duplicate) {
								shots.add(new OddShot(seconds, s.getFullName(),
										channel, s));
							}

							gui.log("[" + s.getScore() + "] " + s.getTitle()
									+ " - " + seconds);

						}

					}
					gui.log("Completed Loop. " + System.currentTimeMillis());

				}
			}, 0, 120 * 1000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
