package me.tuckergardner.oddshot;

import java.awt.BorderLayout;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class BotGUI extends JFrame {
	private Properties prop = new Properties(); // Properties File
	private FileInputStream input; // Input
	private boolean useGUI = false; // Whether or not to use GUI
	private JTextArea statuspane = new JTextArea();
	private JScrollPane scrollpane = new JScrollPane(statuspane);

	public BotGUI() {
		try {
			input = new FileInputStream("config.properties");

			// loads properties file
			prop.load(input);

			String guiString = prop.getProperty("useGui");
			useGUI = Boolean.parseBoolean(guiString);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (useGUI) {
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setTitle("oddshotbot");
			this.setSize(650, 410);

			statuspane.setEditable(false);
			statuspane.setAutoscrolls(true);
			scrollpane.setAutoscrolls(true);

			scrollpane.setSize(650, 200);

			this.add(scrollpane, BorderLayout.CENTER);

			setVisible(true);
		}
	}

	public void log(String line) {
		if (useGUI) {
			statuspane.setText(statuspane.getText() + "\n" + line);
			statuspane.setCaretPosition(statuspane.getDocument().getLength());
		}else{
			System.out.println(line);
		}
	}

}
