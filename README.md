Compiling this with maven requires adding the Jcentral repository

Running: either double click OddShotBot-1.0-jar-with-dependencies.jar, or execute ```java -jar OddShotBot-1.0-jar-with-dependencies.jar```




if you're just double clicking, I recommend setting useGui to ```true```


Obtaining the client id and secret for your account/app can be done by going here: https://www.reddit.com/prefs/apps while logged in to reddit

The bot will check for new posts in whatever subreddit every two minutes, and detect overlapping oddshots if their timestamps are less than 40 seconds apart